package com.sakila.demo.controller;

import com.custom.backend.dto.CountryDto;
import com.sakila.demo.interf.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@RestController
@ComponentScan("com.sakila.demo.interf")
public class CountryController {

    @Autowired
    private CountryService countryService;

    @GetMapping(value = "/countries", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CountryDto>> getCountries() {
       List<CountryDto> countries = countryService.getCountries();
        if(!CollectionUtils.isEmpty(countries)) {
            return new ResponseEntity<>(countries, HttpStatus.ACCEPTED);
        }
       return  new ResponseEntity<List<CountryDto>>(countries, HttpStatus.ACCEPTED);
    }
}
