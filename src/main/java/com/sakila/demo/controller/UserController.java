package com.sakila.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.custom.backend.dto.UserDto;
import com.sakila.demo.config.ApplicationUser;
import com.sakila.demo.interf.UserService;


@RestController
@ComponentScan("com.sakila.demo.interf")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value="/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> getUser(@RequestBody ApplicationUser user) {

        UserDto userDto = userService.getUser(user.getUsername());
        if(null != userDto) {
            return new ResponseEntity<>(userDto, HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(userDto, HttpStatus.NO_CONTENT);
    }
}
