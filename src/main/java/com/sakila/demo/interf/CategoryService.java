package com.sakila.demo.interf;

import com.custom.backend.dto.CategoryDto;
import java.util.List;

public interface CategoryService {

    public List<CategoryDto> categories();
}
