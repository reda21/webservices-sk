package com.sakila.demo.interf;

import com.custom.backend.dto.UserDto;

public interface UserService {

    public UserDto getUser(String userName);
}
