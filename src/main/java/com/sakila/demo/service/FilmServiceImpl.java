
package com.sakila.demo.service;

import com.custom.backend.dto.FilmDto;
import com.custom.backend.service.interf.FilmServiceSakila;
import com.sakila.demo.interf.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("filmService")
public class FilmServiceImpl implements FilmService {

    @Autowired
    private FilmServiceSakila filmServiceSakila;

    public List<FilmDto> getFilms() {
        List<FilmDto> films = filmServiceSakila.getFilms();
        return films;
    }
}

