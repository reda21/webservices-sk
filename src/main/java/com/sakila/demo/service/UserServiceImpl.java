package com.sakila.demo.service;

import com.custom.backend.dto.UserDto;
import com.custom.backend.service.interf.UserServiceSakila;
import com.sakila.demo.interf.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserServiceSakila userServiceSakila;

    @Override
    public UserDto getUser(String userName) {
        return  userServiceSakila.getUser(userName);
    }
}
