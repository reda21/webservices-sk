package com.sakila.demo;

import com.custom.backend.dto.CategoryDto;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.boot.test.web.client.TestRestTemplate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StepDefinitions {

    private TestRestTemplate rest = new TestRestTemplate();

    private List<CategoryDto>  response;

    @When("ask all categories")
    public void ask_all_categories () {
        String uri = "http://localhost:8088/categories";
        response = rest.getForObject(uri, ArrayList.class);
    }

    @Then("i should have n categories")
    public void i_should_have_n_categories() {
        assertEquals(16, response.size());
    }
}
