/**
 *
 */

package com.sakila.demo;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import io.cucumber.junit.Cucumber;
import io.cucumber.spring.CucumberContextConfiguration;

/**
 * @author redinho20
 *
 */
@CucumberContextConfiguration
@SpringBootTest
@RunWith(Cucumber.class)
public class SpringIntegrationTest {


}
